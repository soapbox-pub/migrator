# Soapbox Migrator

![Migrator](migrator.png)

Export data from Mastodon 2.8.4 and import it into Pleroma [2.0.5-plerasstodon](https://git.pleroma.social/alexgleason/pleroma/-/tree/plerasstodon).

### What it migrates
- Users
- Statuses
- Follows / Follow Requests
- Likes
- Mutes
- Blocks
- Lists
- Filters
- Thread Mutes
- Polls / Votes
- Frontend settings for [soapbox-fe](https://gitlab.com/soapbox-pub/soapbox-fe)
- OAuth apps/token

### What it doesn't migrate
- Notifications
- Reports
- Moderation log
- Features exclusive to Mastodon or Pleroma
- Frontend settings for any frontend except soapbox-fe
- Remote data that can be redownloaded
- Some remote data that can't be redownloaded

## Disclaimer

This repo is not for the faint of heart.
If you came here wondering if you can migrate Mastodon to Pleroma, know that it will not be easy.
You should be prepared to learn about the internals of Mastodon/Pleroma, and be willing to edit this repo's code.

Your other option is to just create a new Pleroma server and destroy the old one.
This will be easier in some cases.

That said, this migrator really does work (when operated correctly).
It's designed to be used by large servers and is optimized for speed.
It migrates nearly everything, and can happen very seamlessly.

Long-term, we are aiming to make it as easy as possible, in the hope it can help the Fediverse advance to the next stage.
We believe that Pleroma is the future, and that it will come in all shapes and sizes.

# Performing the migration

The gist of it:

1. Set up the new Pleroma server
2. Copy the `pleroma` folder into your Pleroma repo (merge them)
3. Copy the `mastodon` folder into your Mastodon repo (merge them)
4. Shut down Mastodon
5. Perform the export on the Mastodon server (`rake migrator:export`)
6. While this runs, point your domain name DNS to the new server
7. Copy the exports from the Mastodon server to the Pleroma server
8. Perform the import (`mix migrator.import`)
9. ???
10. Profit

You can **skip steps 4 & 6** to test this without downtime.
I highly recommend you do so.
Don't expect this to work the first time.
Verify the exports manually.

See the `do_migration.sh` script for ideas about making it reproducible.

## How it works

- Mastodon is created with [Ruby on Rails](https://rubyonrails.org/) (Ruby), and Pleroma is created with [Phoenix](https://www.phoenixframework.org/) (Elixir).

- This migrator contains Ruby code used by Mastodon to create an export, and Elixir code used by Pleroma to import the data.

## Philosophy

The theory for this migrator is that it should be hard to export and easy to import.
It's not meant to be a new standard for interoperable Fediverse databases, it's just meant to solve the use case of Mastodon to Pleroma.
It's a one way function.

We also don't intend for this code to ever be merged upstream.
We're trying to solve the easy problem first instead of the hard one, just to prove it can work, and to keep the finish line closer.

So the goal is to make Mastodon export a format that's as close to what Pleroma needs as possible, then Pleroma will just have to read it without (hopefully) doing anything too crazy.

## Advanced usage and Gulp

You will need local repositories of Mastodon and Pleroma.
You must configure them with the needed environment and make sure they start up properly.
The database you're trying to convert should be loaded into Mastodon.
Note that you MUST include `LOCAL_DOMAIN=yourdomain.tld` and `LOCAL_HTTPS=true` in your Mastodon config, or else it will export localhost URLs.
Pleroma probably needs to be configured similarly.

Create a `.env` file and configure the local paths to Mastodon and Pleroma:

```bash
MASTODON_PATH=/home/alex/Projects/mastodon
PLEROMA_PATH=/home/alex/Projects/pleroma
```

Install dependencies:

```bash
yarn
```

You'll then be able to run these commands to migrate the database:

```bash
yarn masto export
yarn pleroma import
```

## Commands

You can also run more granular commands like this:

```bash
yarn masto hello
yarn pleroma hello

yarn masto export:users
yarn pleroma import.users

yarn masto "export:users[local=true]"
yarn masto "export:statuses[local=true&limit=100]"
```

A query syntax is supported, with these parameters:

- `local`
- `limit`

The commands correspond to [Rake](https://github.com/ruby/rake) tasks and [Mix](https://elixirschool.com/en/lessons/basics/mix/) tasks, for Mastodon and Pleroma respectively.

- **Rake** tasks end in `.rake` and are found in `mastodon/`

- **Mix** tasks end in `.ex` and are found in `pleroma/`

The Yarn commands are shorthand for [gulp](https://gulpjs.com/) commands:

```bash
npx gulp masto --task [rakeTask]
npx gulp pleroma --task [mixTask]
```

Which are in turn shorthand for commands like this:

```bash
cd $MASTODON_PATH && bundle exec rake migrator:[rakeTask]

cd $PLEROMA_PATH && mix migrator.[mixTask]
```

Notice how all tasks are in the `migrator` namespace, and the Yarn command conveniently lets you drop them.
The task files still need that namespace, however.

The benefit of using Yarn is that it will automatically copy the tasks from this repo into the correct location before running them, as well as move the exported data afterwards.

## Successful Migrations

### gleasonator.com

On May 15, 2020, [gleasonator.com](https://gleasonator.com) was successfully migrated from Mastodon+Soapbox 2.8.4 to Pleroma 2.0.5-plerasstodon.
It resulted in 1.5 hours of downtime with no obvious side-effects.
All following relationships and important local content were retained, and federation continued working as normally.
Read more about it [in my post](https://gleasonator.com/@alex/posts/9v6cIy69oe757CD3iK).

- 41 local users (and 61,173 remote users)
- 21,932 local statuses
- 18,834 local favourites
- 2,726 follows and follow requests
- 45 local blocks
- 209 mutes
- 26 thread mutes
- 367 local poll votes
- 5 lists
- 17 word filters

Challenges:

- I had to perform the migration twice. The first time I accidentally exported `http://` ActivityPub IDs instead of `https://`, a Federation-breaking mistake.

- It took a long time to download the Mastodon database to my laptop. If I did it again, I'd perform the export directly in the production server and download that instead, because the exports don't contain RSA keys for remote users.

- Logged-in users were kicked out of their session after the move, and had to log in again. It might be possible to migrate sessions, but I haven't looked into it yet.

The actual script I used to perform this process can be found in this repo, [`./do_migration.sh`](do_migration.sh).
I ran it multiple times, destroying the database in between, until I was confident about pointing the DNS at the new server.

### neenster.org

An extremely smooth migration.
It took only 9 minutes of total downtime, and users remained logged in afterwards.

This time, instead of using my laptop as an intermediary between the two servers, I ran the export directly on the Mastodon server, moved the dumps straight to the Pleroma server, then ran the import directly on the Pleroma server.
The old way took 28 minutes, and this way took only 9 minutes.
During the 9 minutes of downtime, I switched the DNS over and got a new SSL cert, so it was completely seamless.

Another improvement is that I got OAuth apps/tokens migrating, so I was able to keep users logged in. This did require [hacking the frontend](https://gitlab.com/soapbox-pub/soapbox-legacy/-/commit/116e886021587fd4c5f01047735da776e64bccf1) of the Mastodon server a week beforehand.

- 56,543 local and remote users
- 3,319 local statuses
- 5,319 local favourites
- Other stuff I didn't count

Challenges:

- After the switch I tested media uploads and they didn't work. I spent at least 2 hours messing with different S3 upload settings before realizing it was actually an incompatibility between an Elixir dependency and the runtime environment... ugh. Patching my Pleroma repo (upgrading the Elixir dependency) fixed it.

### More?

If you've successfully migrated Mastodon to Pleroma with this tool, you're welcome to open a merge request to document your experience here.

Other migrations are planned, including:

- glindr.org
- spinster.xyz
