echo "===== Moving exports to Pleroma server"
ssh root@spinster-pleroma "sudo -Hu pleroma mkdir -p /opt/pleroma/migrator"
ssh root@spinster-pleroma "sudo -Hu pleroma rsync -av gabsocial@spinster-mastodon:~/live/migrator /opt/pleroma"

echo "===== Copying importer tasks to Pleroma server"
rsync -av ../../pleroma/ root@spinster-pleroma:/opt/pleroma
ssh root@spinster-pleroma "chown -R pleroma:pleroma /opt/pleroma"

echo "===== Importing data into Pleroma"
ssh root@spinster-pleroma "cd /opt/pleroma && sudo -Hu pleroma MIX_ENV=prod mix migrator.import.votes"
ssh root@spinster-pleroma "cd /opt/pleroma && sudo -Hu pleroma MIX_ENV=prod mix migrator.import.thread_mutes"
ssh root@spinster-pleroma "cd /opt/pleroma && sudo -Hu pleroma MIX_ENV=prod mix migrator.import.likes"
ssh root@spinster-pleroma "cd /opt/pleroma && sudo -Hu pleroma MIX_ENV=prod mix migrator.import.statuses"
