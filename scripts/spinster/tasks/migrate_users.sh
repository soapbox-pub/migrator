echo "===== Exporting Mastodon user data"
ssh gabsocial@spinster-mastodon "cd ~/live && RAILS_ENV=production /home/gabsocial/.rbenv/shims/bundle exec rake migrator:export:users" &
ssh gabsocial@spinster-mastodon "cd ~/live && RAILS_ENV=production /home/gabsocial/.rbenv/shims/bundle exec rake migrator:export:follows" &
ssh gabsocial@spinster-mastodon "cd ~/live && RAILS_ENV=production /home/gabsocial/.rbenv/shims/bundle exec rake 'migrator:export:blocks[local=true]'" &
ssh gabsocial@spinster-mastodon "cd ~/live && RAILS_ENV=production /home/gabsocial/.rbenv/shims/bundle exec rake migrator:export:mutes" &
ssh gabsocial@spinster-mastodon "cd ~/live && RAILS_ENV=production /home/gabsocial/.rbenv/shims/bundle exec rake migrator:export:lists" &
ssh gabsocial@spinster-mastodon "cd ~/live && RAILS_ENV=production /home/gabsocial/.rbenv/shims/bundle exec rake migrator:export:filters" &
ssh gabsocial@spinster-mastodon "cd ~/live && RAILS_ENV=production /home/gabsocial/.rbenv/shims/bundle exec rake migrator:export:apps" &
ssh gabsocial@spinster-mastodon "cd ~/live && RAILS_ENV=production /home/gabsocial/.rbenv/shims/bundle exec rake migrator:export:tokens" &
wait

echo "===== Moving exports to Pleroma server"
ssh root@spinster-pleroma "sudo -Hu pleroma mkdir -p /opt/pleroma/migrator"
ssh root@spinster-pleroma "sudo -Hu pleroma rsync -av gabsocial@spinster-mastodon:~/live/migrator /opt/pleroma"

echo "===== Importing data into Pleroma"
ssh root@spinster-pleroma "cd /opt/pleroma && sudo -Hu pleroma MIX_ENV=prod mix migrator.import.users"
ssh root@spinster-pleroma "cd /opt/pleroma && sudo -Hu pleroma MIX_ENV=prod mix migrator.import.follows" &
ssh root@spinster-pleroma "cd /opt/pleroma && sudo -Hu pleroma MIX_ENV=prod mix migrator.import.blocks" &
ssh root@spinster-pleroma "cd /opt/pleroma && sudo -Hu pleroma MIX_ENV=prod mix migrator.import.mutes" &
ssh root@spinster-pleroma "cd /opt/pleroma && sudo -Hu pleroma MIX_ENV=prod mix migrator.import.lists" &
ssh root@spinster-pleroma "cd /opt/pleroma && sudo -Hu pleroma MIX_ENV=prod mix migrator.import.filters" &
ssh root@spinster-pleroma "cd /opt/pleroma && sudo -Hu pleroma MIX_ENV=prod mix migrator.import.apps"
ssh root@spinster-pleroma "cd /opt/pleroma && sudo -Hu pleroma MIX_ENV=prod mix migrator.import.tokens"
wait

echo "===== Upgrading Pleroma"
ssh root@spinster-pleroma "rm -rf /opt/pleroma/lib/mix/tasks/migrator"
ssh root@spinster-pleroma "rm -f /opt/pleroma/lib/mix/migrator.ex"

ssh root@spinster-pleroma "cd /opt/pleroma && sudo -Hu pleroma git reset --hard HEAD"
ssh root@spinster-pleroma "cd /opt/pleroma && sudo -Hu pleroma git checkout develop"

ssh root@spinster-pleroma "cd /opt/pleroma && sudo -Hu pleroma MIX_ENV=prod mix deps.get"
ssh root@spinster-pleroma "cd /opt/pleroma && sudo -Hu pleroma MIX_ENV=prod mix ecto.migrate"

echo "===== Starting Pleroma sever"
ssh root@spinster-pleroma "systemctl start pleroma"
