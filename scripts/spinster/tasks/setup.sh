echo "===== Cleaning Mastodon server"
ssh root@spinster-mastodon "rm -rf /home/gabsocial/live/migrator"

echo "===== Cleaning Pleroma server"
ssh root@spinster-pleroma "systemctl stop pleroma"

ssh root@spinster-pleroma "rm -rf /opt/pleroma/lib/mix/tasks/migrator"
ssh root@spinster-pleroma "rm -f /opt/pleroma/lib/mix/migrator.ex"
ssh root@spinster-pleroma "rm -rf /opt/pleroma/migrator"

ssh root@spinster-pleroma "cd /opt/pleroma && sudo -Hu pleroma git reset --hard HEAD"
ssh root@spinster-pleroma "cd /opt/pleroma && sudo -Hu pleroma git checkout plerasstodon"

ssh root@spinster-pleroma "sudo -Hiu postgres dropdb --if-exists pleroma"
ssh root@spinster-pleroma "sudo -Hiu postgres psql postgres -f /opt/pleroma/config/setup_db.psql"

ssh root@spinster-pleroma "cd /opt/pleroma && sudo -Hu pleroma MIX_ENV=prod mix deps.get"
ssh root@spinster-pleroma "cd /opt/pleroma && sudo -Hu pleroma MIX_ENV=prod mix ecto.migrate"

ssh root@spinster-pleroma "cd /opt/pleroma && sudo -Hu pleroma MIX_ENV=prod mix pleroma.config migrate_to_db"

echo "===== Copying migrator tasks to each server"
rsync -av ../../mastodon/ gabsocial@spinster-mastodon:~/live
rsync -av ../../pleroma/ root@spinster-pleroma:/opt/pleroma
ssh root@spinster-pleroma "chown -R pleroma:pleroma /opt/pleroma"
