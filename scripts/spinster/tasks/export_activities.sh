echo "===== Exporting Mastodon activity data"
ssh gabsocial@spinster-mastodon "cd ~/live && RAILS_ENV=production /home/gabsocial/.rbenv/shims/bundle exec rake 'migrator:export:statuses[local=true]'" &
ssh gabsocial@spinster-mastodon "cd ~/live && RAILS_ENV=production /home/gabsocial/.rbenv/shims/bundle exec rake 'migrator:export:likes[local=true]'" &
ssh gabsocial@spinster-mastodon "cd ~/live && RAILS_ENV=production /home/gabsocial/.rbenv/shims/bundle exec rake 'migrator:export:thread_mutes'" &
ssh gabsocial@spinster-mastodon "cd ~/live && RAILS_ENV=production /home/gabsocial/.rbenv/shims/bundle exec rake 'migrator:export:votes[local=true]'" &
wait
