START=$(date)
echo "Start: $START"

./tasks/setup.sh

# echo "===== Shutting down Mastodon"
# ssh root@spinster-mastodon "systemctl stop gabsocial-*"
# sleep 10

./tasks/migrate_users.sh &
./tasks/export_activities.sh &
wait

./tasks/import_activities.sh

echo "Start: $START"
echo "End: $(date)"
