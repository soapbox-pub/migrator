require('dotenv').config();
import { src, dest, series, parallel } from 'gulp';
import { join } from 'path';
import { spawn } from 'child_process';
import { argv } from 'yargs';

const { MASTODON_PATH, PLEROMA_PATH } = process.env;

const runCmd = (path, cmd, done) => {
  const s = spawn('bash');
  s.stdin.end(`cd "${path}" && ${cmd}`);
  s.stdout.on('data', data => console.log(data.toString()));
  s.stderr.on('data', data => console.error(data.toString()));
  s.on('error', error => console.error(error.message));
  s.on('close', code => {
    console.log(`child process exited with code ${code}`);
    done();
  });
}

const copyMastoFiles   = () => src('mastodon/**').pipe(dest(MASTODON_PATH));
const copyPleromaFiles = () => src('pleroma/**' ).pipe(dest(PLEROMA_PATH));

const moveExports = () => src(join(MASTODON_PATH, 'migrator/**'))
  .pipe(dest(join(PLEROMA_PATH, 'migrator/')));

const rake = done => {
  runCmd(MASTODON_PATH, `bundle exec rake "migrator:${argv.task}"`, done);
}

const mix = done => {
  runCmd(PLEROMA_PATH, `mix "migrator.${argv.task}"`, done);
}

const rspec = done => {
  runCmd(MASTODON_PATH, 'RAILS_ENV=test bundle exec rspec ./spec/migrator/', done);
}

export const masto   = series(copyMastoFiles, rake);
export const pleroma = series(moveExports, copyPleromaFiles, mix);
export const testMasto = series(copyMastoFiles, rspec);
