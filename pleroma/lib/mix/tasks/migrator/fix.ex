defmodule Mix.Tasks.Migrator.Fix do
  use Mix.Task
  alias Pleroma.Object
  alias Pleroma.Repo

  require Logger
  import Ecto.Changeset
  import Ecto.Query
  import Mix.Pleroma

  @shortdoc "Fix stuff from old migrations. Run at your own risk."

  # Apparently media can get migrated in reverse order.
  # This reverses the order of media attachments in all migrated objects.
  # https://gitlab.com/soapbox-pub/migrator/-/issues/27
  def run(["reverse_media"]) do
    start_pleroma()

    stream =
      Object
      |> where([o], fragment("?->'pleroma_internal'->>'migrator'='true'", o.data))
      |> where([o], fragment("json_array_length((?->'attachment')::json) > 1", o.data))
      |> Repo.stream()

    Repo.transaction(fn ->
      Enum.each(stream, fn object ->
        with %Object{data: %{"id" => id} = data} <- object do
          shell_info("Reversing media for #{id}")

          object
          |> change(data: do_reverse_media(data))
          |> Repo.update!()
        end
      end)
    end, timeout: :infinity)
  end

  defp do_reverse_media(%{"attachment" => attachments} = data) when is_list(attachments) do
    Map.put(data, "attachment", Enum.reverse(attachments))
  end

  defp do_reverse_media(data), do: data
end
