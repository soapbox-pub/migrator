defmodule Mix.Tasks.Migrator.Import.Apps do
  use Mix.Task
  import Mix.Pleroma
  import Mix.Migrator
  alias Pleroma.Repo
  alias Pleroma.Web.OAuth.App

  @shortdoc "Import OAuth apps."
  def run(_) do
    start_pleroma()
    File.stream!("migrator/apps.txt")
    |> Enum.each(&handle_line/1)
  end

  defp handle_line(line) do
    Jason.decode!(line)
    |> keys_to_atoms()
    |> loop_fields([:inserted_at, :updated_at], &parse_timestamp/1)
    |> try_create_app()
  end

  defp try_create_app(params) do
    changeset = struct(App, params)

    with {:ok, app} <- Repo.insert(changeset) do
      shell_info("App #{app.client_name} created")
    else
      _ -> shell_info("Could not create app")
    end
  end
end
