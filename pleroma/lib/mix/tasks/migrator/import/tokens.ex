defmodule Mix.Tasks.Migrator.Import.Tokens do
  use Mix.Task
  import Mix.Pleroma
  import Mix.Migrator
  alias Pleroma.Repo
  alias Pleroma.User
  alias Pleroma.Web.OAuth.App
  alias Pleroma.Web.OAuth.Token

  @shortdoc "Import OAuth tokens."
  def run(_) do
    start_pleroma()
    File.stream!("migrator/tokens.txt")
    |> Enum.each(&handle_line/1)
  end

  defp handle_line(line) do
    Jason.decode!(line)
    |> keys_to_atoms()
    |> loop_fields([:inserted_at, :updated_at], &parse_timestamp/1)
    |> try_create_token()
  end

  defp try_create_token(params) do
    changeset = struct(Token, params)

    with %{user_ap_id: user_ap_id} <- params,
         %{app_client_id: app_client_id} <- params,
         %User{id: user_id} = _user <- User.get_by_ap_id(user_ap_id),
         %App{id: app_id} = _app <- Repo.get_by(App, client_id: app_client_id),
         changeset <- Map.delete(changeset, :user_ap_id),
         changeset <- Map.delete(changeset, :app_client_id),
         changeset <- Map.merge(changeset, %{user_id: user_id, app_id: app_id}),
         {:ok, token} <- Repo.insert(changeset) do
      shell_info("Token #{token.id} created")
    else
      _ -> shell_info("Could not create token")
    end
  end
end
