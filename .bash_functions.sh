# Some CLI functions to make development feel less insane.
# Mainly for me, but feel free to try to use them.
# Just source it in with .bashrc or .zshrc
#
#   source /path/here/.bash_functions.sh
#

MIGRATOR_PATH=~/Projects/migrator
source "$MIGRATOR_PATH/.env"

reset-pleroma() {
  sudo -Hu postgres dropdb --if-exists pleroma &&
  cd "$PLEROMA_PATH" &&
  sudo -Hu postgres psql postgres -f config/setup_db.psql &&
  mix ecto.migrate
}

migrator() {
  reset-pleroma &&
  cd "$MIGRATOR_PATH" &&
  yarn masto "export[$1]" &&
  yarn pleroma import &&
  cd "$PLEROMA_PATH" &&
  mix phx.server
}

migrator_prod() {
  export MIX_ENV="prod"
  reset-pleroma &&
  cd "$MIGRATOR_PATH" &&
  yarn masto export &&
  yarn pleroma import
}
