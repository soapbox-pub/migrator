# For the sake of completion, this is the script I used to perform the
# actual migration on neenster.org

export MIX_ENV="prod"

START=$(date)
echo "Start: $START"

# Clean Pleroma server
echo "===== Cleaning Pleroma server"
ssh root@neenster-new "systemctl stop pleroma"
ssh root@neenster-new "sudo -Hu postgres dropdb pleroma"
ssh root@neenster-new "sudo -Hu postgres psql postgres -f /opt/pleroma/config/setup_db.psql"
ssh root@neenster-new "cd /opt/pleroma && sudo -Hu pleroma MIX_ENV=prod mix ecto.migrate"

# Perform export on Mastodon server
echo "===== Exporting Mastodon data"
rsync -av mastodon/ gabsocial@neenster-old:~/live
ssh gabsocial@neenster-old "cd ~/live && RAILS_ENV=production /home/gabsocial/.rbenv/shims/bundle exec rake migrator:export"

# Copy data from Mastodon->Pleroma server
echo "===== Moving exports to Pleroma server"
ssh root@neenster-new "sudo -Hu pleroma mkdir -p /opt/pleroma/migrator"
ssh root@neenster-new "sudo -Hu pleroma rsync -av gabsocial@167.71.101.253:~/live/migrator /opt/pleroma"

# Perform import on Pleroma server
echo "===== Importing data into Pleroma"
rsync -av pleroma/ root@neenster-new:/opt/pleroma
ssh root@neenster-new "chown -R pleroma:pleroma /opt/pleroma"
ssh root@neenster-new "cd /opt/pleroma && sudo -Hu pleroma MIX_ENV=prod mix migrator.import"

# Start the Pleroma server
echo "===== Starting Pleroma sever"
ssh root@neenster-new "systemctl start pleroma"

echo "Start: $START"
echo "End: $(date)"
