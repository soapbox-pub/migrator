# frozen_string_literal: true

namespace :migrator do
  desc 'Test that Mastodon can run this task.'
  task :hello => :environment do
    puts 'Hello, World!'
  end
end
