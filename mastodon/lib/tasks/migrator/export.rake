# frozen_string_literal: true

namespace :migrator do
  desc 'Export full database'
  task :export, [:query] => :environment do |task, args|
    include Migrator

    puts "Running full export with recommended settings"
      do_migrator('users')
      do_migrator('statuses', 'local=true')
      do_migrator('likes', 'local=true')
      do_migrator('follows')
      do_migrator('blocks', 'local=true')
      do_migrator('mutes')
      do_migrator('thread_mutes')
      do_migrator('votes', 'local=true')
      do_migrator('lists')
      do_migrator('filters')
      do_migrator('apps')
      do_migrator('tokens')
    puts 'Export finished.'
  end
end

def do_migrator(task, query = nil)
  Rake::Task["migrator:export:#{task}"].invoke(query)
end
