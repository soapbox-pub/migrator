# frozen_string_literal: true

namespace :migrator do
  namespace :export do
    desc 'Export OAuth tokens'
    task :tokens, [:query] => :environment do |task, args|
      include Migrator

      open_file('migrator/tokens.txt') do |file|
        args = parse_args(args)
        puts "Exporting tokens with args: #{args}"

        get_query(Doorkeeper::AccessToken, args).eager_load(:application)
        .find_each do |t|
          puts "Exporting token #{t.id}"
          if t.resource_owner_id
            file.puts(Migrator::TokenSerializer.to_hash(t).to_json)
          else
            puts "Token #{t.id} is malformed. Skipping."
          end
        end
      end
    end
  end
end
