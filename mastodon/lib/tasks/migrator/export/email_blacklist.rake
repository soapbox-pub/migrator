# frozen_string_literal: true

namespace :migrator do
  namespace :export do
    desc 'Export email blacklist'
    task :email_blacklist, [:query] => :environment do |task, args|
      include Migrator

      open_file('migrator/email_blacklist.txt') do |file|
        args = parse_args(args)
        puts "Exporting email blacklist with args: #{args}"

        list = EmailDomainBlock.all.map do |e| e.domain end
        file.puts(list.to_json)
      end
    end
  end
end
