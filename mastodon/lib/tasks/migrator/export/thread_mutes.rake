# frozen_string_literal: true

namespace :migrator do
  namespace :export do
    desc 'Export thread mutes'
    task :thread_mutes, [:query] => :environment do |task, args|
      include Migrator

      open_file('migrator/thread_mutes.txt') do |file|
        args = parse_args(args)
        puts "Exporting thread mutes with args: #{args}"

        get_query(ConversationMute, args).eager_load(:account, :conversation)
        .find_each do |m|
          puts "Exporting mute on thread #{m.conversation_id} by @#{m.account.acct}"
          file.puts(Migrator::ThreadMuteSerializer.to_hash(m).to_json)
        end
      end
    end
  end
end
