# frozen_string_literal: true

namespace :migrator do
  namespace :export do
    desc 'Export lists'
    task :lists, [:query] => :environment do |task, args|
      include Migrator

      open_file('migrator/lists.txt') do |file|
        args = parse_args(args)
        puts "Exporting lists with args: #{args}"

        get_query(List, args).eager_load(:account, :accounts)
        .find_each do |l|
          puts "Exporting list \"#{l.title}\" by @#{l.account.acct}"
          file.puts(Migrator::ListSerializer.to_hash(l).to_json)
        end
      end
    end
  end
end
