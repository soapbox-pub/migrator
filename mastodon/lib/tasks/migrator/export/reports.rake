# frozen_string_literal: true

namespace :migrator do
  namespace :export do
    desc 'Export reports'
    task :reports, [:query] => :environment do |task, args|
      include Migrator

      open_file('migrator/reports.txt') do |file|
        args = parse_args(args)
        puts "Exporting reports with args: #{args}"

        get_query(Report, args).find_each do |r|
          file.puts(Migrator::ReportSerializer.to_hash(r).to_json)
        end
      end
    end
  end
end
