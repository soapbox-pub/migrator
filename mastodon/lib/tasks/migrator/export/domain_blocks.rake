# frozen_string_literal: true

namespace :migrator do
  namespace :export do
    desc 'Export domain blocks'
    task :domain_blocks, [:query] => :environment do |task, args|
      include Migrator

      open_file('migrator/domain_blocks.txt') do |file|
        args = parse_args(args)
        puts "Exporting domain blocks with args: #{args}"

        hash = {
          media_removal: DomainBlock.where(reject_media: true).map do |b| b.domain end,
          reject: DomainBlock.where(severity: "suspend").map do |b| b.domain end,
          followers_only: DomainBlock.where(severity: "silence").map do |b| b.domain end,
          report_removal: DomainBlock.where(reject_reports: true).map do |b| b.domain end,
        }

        file.puts(hash.to_json)
      end
    end
  end
end
