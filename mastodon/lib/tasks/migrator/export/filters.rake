# frozen_string_literal: true

namespace :migrator do
  namespace :export do
    desc 'Export filters'
    task :filters, [:query] => :environment do |task, args|
      include Migrator
      users = {}

      open_file('migrator/filters.txt') do |file|
        args = parse_args(args)
        puts "Exporting filters with args: #{args}"

        get_query(CustomFilter, args).eager_load(:account).find_each do |f|
          hash = Migrator::FilterSerializer.to_hash(f)
          ap_id = hash[:user_ap_id]
          users[ap_id] ||= 0
          users[ap_id] += 1
          hash[:filter_id] = users[ap_id]
          puts "Exporting filter #{f.id} by @#{f.account.acct}"
          file.puts(hash.to_json)
        end
      end
    end
  end
end
