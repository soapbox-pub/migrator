# frozen_string_literal: true

namespace :migrator do
  namespace :export do
    desc 'Export OAuth apps'
    task :apps, [:query] => :environment do |task, args|
      include Migrator

      open_file('migrator/apps.txt') do |file|
        args = parse_args(args)
        puts "Exporting apps with args: #{args}"

        get_query(Doorkeeper::Application, args)
        .find_each do |app|
          puts "Exporting app #{app.name}"
          file.puts(Migrator::AppSerializer.to_hash(app).to_json)
        end
      end
    end
  end
end
