# frozen_string_literal: true

namespace :migrator do
  namespace :export do
    desc 'Export statuses'
    task :statuses, [:query] => :environment do |task, args|
      include Migrator

      open_file('migrator/statuses.txt') do |file|
        args = parse_args(args)
        puts "Exporting statuses with args: #{args}"

        i = 0
        query = get_query(Status, args)
        total = query.count

        query.eager_load(
          :account, :conversation, :tags,
          :in_reply_to_account, :media_attachments, :thread, :poll,
          favourites: [:account], reblogs: [:account], reblog: [:account],
          active_mentions: [:account])
        .find_each do |s|
          i += 1
          counter = "(#{i}/#{total})"
          puts "#{counter} Exporting status #{s.id} by @#{s.account.acct}"
          file.puts(Migrator::StatusSerializer.to_hash(s).to_json)
        end
      end
    end
  end
end
