# frozen_string_literal: true

namespace :migrator do
  namespace :export do
    desc 'Export poll votes'
    task :votes, [:query] => :environment do |task, args|
      include Migrator

      open_file('migrator/votes.txt') do |file|
        args = parse_args(args)
        puts "Exporting poll votes with args: #{args}"

        get_query(PollVote, args).eager_load(
          :account, poll: [:account, :status])
        .find_each do |v|
          puts "Exporting vote #{v.id} on poll #{v.poll_id} by @#{v.account.acct}"
          file.puts(Migrator::VoteSerializer.to_hash(v).to_json)
        end
      end
    end
  end
end
