# frozen_string_literal: true

namespace :migrator do
  namespace :export do
    desc 'Export blocks'
    task :blocks, [:query] => :environment do |task, args|
      include Migrator

      open_file('migrator/blocks.txt') do |file|
        args = parse_args(args)
        puts "Exporting blocks with args: #{args}"

        get_query(Block, args).eager_load(:target_account, :account)
        .find_each do |b|
          puts "Exporting block on @#{b.target_account.acct} from @#{b.account.acct}"
          file.puts(Migrator::BlockSerializer.to_hash(b).to_json)
        end
      end
    end
  end
end
