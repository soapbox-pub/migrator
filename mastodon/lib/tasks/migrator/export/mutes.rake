# frozen_string_literal: true

namespace :migrator do
  namespace :export do
    desc 'Export mutes'
    task :mutes, [:query] => :environment do |task, args|
      include Migrator

      open_file('migrator/mutes.txt') do |file|
        args = parse_args(args)
        puts "Exporting mutes with args: #{args}"

        get_query(Mute, args).eager_load(:target_account, :account)
        .find_each do |m|
          puts "Exporting mute on @#{m.target_account.acct} from @#{m.account.acct}"
          file.puts(Migrator::MuteSerializer.to_hash(m).to_json)
        end
      end
    end
  end
end
