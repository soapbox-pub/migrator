# frozen_string_literal: true

namespace :migrator do
  namespace :export do
    desc 'Export accounts and users'
    task :users, [:query] => :environment do |task, args|
      include Migrator

      open_file('migrator/users.txt') do |file|
        args = parse_args(args)
        puts "Exporting users with args: #{args}"

        i = 0
        query = get_query(Account, args)
        total = query.count

        query
        .eager_load(:domain_blocks, :account_stat, :user, status_pins: [:status])
        .find_each do |a|
          i += 1
          counter = "(#{i}/#{total})"
          puts "#{counter} Exporting user @#{a.acct}"
          file.puts(Migrator::UserSerializer.to_hash(a).to_json)
        end
      end
    end
  end
end
