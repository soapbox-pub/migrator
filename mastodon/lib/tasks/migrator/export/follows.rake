# frozen_string_literal: true

namespace :migrator do
  namespace :export do
    desc 'Export follows'
    task :follows, [:query] => :environment do |task, args|
      include Migrator

      open_file('migrator/follows.txt') do |file|
        args = parse_args(args)
        puts "Exporting follows with args: #{args}"

        preload = [:target_account, :account]

        get_query(Follow, args).eager_load(preload).find_each do |f|
          puts "Exporting follow to @#{f.target_account.acct} from @#{f.account.acct}"
          file.puts(Migrator::FollowSerializer.to_hash(f).to_json)
        end

        get_query(FollowRequest, args).eager_load(preload).find_each do |f|
          puts "Exporting follow request to @#{f.target_account.acct} from @#{f.account.acct}"
          file.puts(Migrator::FollowSerializer.to_hash(f).to_json)
        end
      end
    end
  end
end
