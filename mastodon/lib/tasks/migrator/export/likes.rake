# frozen_string_literal: true

namespace :migrator do
  namespace :export do
    desc 'Export likes'
    task :likes, [:query] => :environment do |task, args|
      include Migrator

      open_file('migrator/likes.txt') do |file|
        args = parse_args(args)
        puts "Exporting likes with args: #{args}"

        i = 0
        query = get_query(Favourite, args)
        total = query.count

        query.eager_load(
          :account,
          status: [:account, :conversation, mentions: [:account]])
        .find_each do |f|
          i += 1
          counter = "(#{i}/#{total})"
          puts "#{counter} Exporting like #{f.id} by @#{f.account.acct}"
          file.puts(Migrator::LikeSerializer.to_hash(f).to_json)
        end
      end
    end
  end
end
