# frozen_string_literal: true

require 'rack/utils'

module Migrator
  include RoutingHelper

  def open_file(filename)
    FileUtils.mkdir_p filename.split("/")[0..-2].join("/")
    File.delete(filename) if File.exists? filename
    File.open(filename, 'a') do |file|
      yield file
    end
  end

  def parse_dt(dt)
    dt&.strftime("%Y-%m-%dT%T.%6NZ")
  end

  def deep_copy(o)
    Marshal.load(Marshal.dump(o))
  end

  def get_limit(args)
    args[:limit] ? Integer(args[:limit]) : nil
  end

  def parse_args(args)
    query = Rack::Utils.parse_nested_query(args[:query]).to_hash.symbolize_keys
    query[:limit] = int_or_nil(query[:limit])
    query[:local] = bool(query[:local])
    query.freeze
  end

  def get_query(model, args)
    query = model
    if args[:local]
      query = query.where(domain: nil) if model == Account
      query = query.where(local: true) if model == Status
      query = query.joins(:account).where('accounts.domain': nil) if [
        Favourite, Block, PollVote
      ].include? model
    end
    query.limit(args[:limit])
  end

  def actor_uri(a)
    a.local? ? account_url(a) : a.uri
  end

  def actor_followers_uri(a)
    a.local? ? account_followers_url(a) : a.followers_url
  end

  def context_uri(c)
    # FIXME: Fallback for Mastodon 3.0+ where OStatus is removed
    c.uri || OStatus::TagManager.instance.unique_tag(c.created_at, c.id, 'Conversation')
  end

  def status_context(status)
    c = status.conversation
    status.local ? context_uri(c) : c.uri
  end

  def masto_serialize(serializer, object)
    published = object.respond_to?(:created_at) ? object.created_at : nil
    data = ActiveModelSerializers::SerializableResource.new(
      object,
      serializer: serializer,
      adapter: ActivityPub::Adapter
    ).serializable_hash
    data.delete('@context')
    data[:pleroma_internal] = internal_tag()
    data[:published] = published if published
    if data[:object].is_a? Hash
      data.delete('@context')
      data[:object][:to] = data[:to] || []
      data[:object][:cc] = data[:cc] || []
      data[:object][:actor] = data[:actor]
      data[:object][:pleroma_internal] = internal_tag()
      data[:object][:published] = published if published
    end
    data
  end

  private

  def internal_tag()
    {
      migrator: true
    }
  end

  def int_or_nil(val)
    val ? val.to_i : nil
  end

  def bool(val)
    val == 'true'
  end
end
