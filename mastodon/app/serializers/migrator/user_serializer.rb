class Migrator::UserSerializer
  class << self
    include Migrator

    def to_hash(a)
      u = a&.user
      s = u ? get_user_settings(u) : {}
      {
        # id: a.id,
        email: u&.email || u&.unconfirmed_email,
        password_hash: a.local? ? u&.encrypted_password : nil,
        name: make_display_name(a),
        nickname: a.acct,
        bio: a.local? ? Formatter.instance.simplified_format(a) : a.note,
        inserted_at: parse_dt(a.created_at),
        updated_at: parse_dt(a.updated_at),
        ap_id: actor_uri(a), # Pleroma should override for local users
        avatar: a.avatar? ? make_avatar(a) : nil,
        local: a.local?,
        follower_address: actor_followers_uri(a), # Pleroma should override for local users
        # last_refreshed_at: parse_dt(a.last_webfingered_at), # Omit to force refresh of remote users
        tags: make_tags(a),
        last_digest_emailed_at: parse_dt(u&.last_emailed_at),
        following_address: a.local? ? account_follow_url(a) : nil, # FIXME: No remote equivalent? # Pleroma should override for local users
        keys: a.private_key&.chomp,
        banner: a.header? ? make_banner(a) : nil,
        background: nil, # No equivalent
        source_data: make_source_data(a),
        note_count: a.account_stat.statuses_count,
        follower_count: a.account_stat.followers_count,
        following_count: a.account_stat.following_count,
        locked: a.locked,
        confirmation_pending: !u&.confirmed?,
        # password_reset_pending: u&.reset_password_sent_at.present?,
        # confirmation_token: u&.reset_password_token,
        default_scope: s[:default_privacy],
        blocks: nil, # deprecated (replaced with `blocked_users` relation)
        domain_blocks: a.local? ? a.domain_blocks.map { |b| b.domain } : nil,
        mutes: nil, # deprecated (replaced with `muted_users` relation)
        muted_reblogs: nil, # deprecated (replaced with `reblog_muted_users` relation)
        muted_notifications: nil, # deprecated (replaced with `notification_muted_users` relation)
        subscribers: nil, # deprecated (replaced with `subscriber_users` relation)
        deactivated: u&.disabled || a.suspended_at != nil,
        no_rich_text: nil, # No equivalent
        ap_enabled: a.local? ? false : true, # Kind of a weird thing, but it's what Pleroma does
        is_moderator: u&.moderator,
        is_admin: u&.admin,
        show_role: nil, # No equivalent
        settings: nil, # MastoFE settings. Not relevant to us.
        magic_key: nil, # Unused: https://git.pleroma.social/pleroma/pleroma/-/issues/1737
        uri: a.url,
        hide_followers_count: nil, # No equivalent
        hide_follows_count: nil, # No equivalent
        hide_followers: nil, # No equivalent
        hide_follows: nil, # No equivalent
        hide_favorites: nil, # No equivalent
        unread_conversation_count: nil, # FIXME: Needs a function. Not possible with pure Mastodon (but it is with Mastodon+Soapbox), though probably not worth the trouble.
        # pinned_activities: a.status_pins.map { |p| "#{p.status.id}" }, # Dropping this now that we've dropped IDs from activities
        email_notifications: s[:notification_emails], # Only supports "digest", others will be lost.
        mascot: nil, # No equivalent
        emoji: nil, # Unused by Pleroma 2.0.3, but will be soon
        pleroma_settings_store: u ? make_fe_settings(u, s) : nil,
        fields: make_fields(a),
        raw_fields: a.local? ? a['fields'] : nil,
        discoverable: a.discoverable,
        invisible: nil, # No equivalent
        notification_settings: make_notification_settings(s),
        skip_thread_containment: nil, # No equivalent
        also_known_as: a.also_known_as,
        allow_following_move: nil, # Not in 2.8.4, but might be relevant for newer Mastodons
        actor_type: a.actor_type,

        # Not in 2.0.3 but in `develop`
        # public_key: a.public_key,
        # inbox: a.inbox_url,
        # shared_inbox: a.shared_inbox_url,
      }.compact # `compact` removes all keys with nil values
    end

    private

    def make_display_name(a)
      # Pleroma requires display names, Mastodon doesn't.
      # Mastodon users with empty display names will expect to see their username.
      return a.display_name if !a.display_name.blank?
      a.local? ? a.username : nil
    end

    def make_avatar(a)
      {
        url: [{
          href: a.local? ? full_asset_url(a.avatar.url(:original)) : a.avatar_remote_url,
          type: a.local? ? "Link" : nil,
          mediaType: a.avatar_content_type,
        }.compact],
        name: a.avatar_file_name,
        type: "Image",
      }.compact
    end

    def make_banner(a)
      {
        url: [{
          href: a.local? ? full_asset_url(a.header.url(:original)) : a.header_remote_url,
          type: a.local? ? "Link" : nil,
          mediaType: a.header_content_type,
        }.compact],
        name: a.header_file_name,
        type: "Image",
      }.compact
    end

    def make_fields(a)
      return [] if a['fields'] == {}
      a.local? ? render_fields(a) : a['fields']
    end

    def render_fields(a)
      fields = a['fields']
      fields&.map do |field|
        {
          name: field['name'],
          value: Formatter.instance.format_field(a, field['value'])
        }
      end
    end

    def make_notification_settings(s)
      # Making some assumptions about how this works
      # https://git.pleroma.social/pleroma/pleroma/-/issues/1738
      i = s[:interactions]
      return nil if !i
      {
        follows: true,
        followers: true,
        non_follows: !i['must_be_following'],
        non_followers: !i['must_be_follower']
      }
    end

    def make_source_data(a)
      return nil if !a.local?
      return nil if a.emojis.blank?
      { tag: make_emoji(a) }
    end

    def make_emoji(a)
      a.emojis.map do |e|
        # This format will change in a future Pleroma version
        # https://git.pleroma.social/pleroma/pleroma/-/commit/9172d719ccbf84d55236007d329fc880db69fe42
        {
          type: 'Emoji',
          icon: {
            type: 'Image',
            url: a.local? ? full_asset_url(e.image.url) : e.image_remote_url
          },
          name: ":#{e.shortcode}:"
        }
      end
    end

    def make_fe_settings(u, s)
      return nil if !u
      {
        # masto_fe: get_web_settings(u),
        soapbox_fe: make_soapbox_fe_settings(u, s)
      }.compact
    end

    def make_soapbox_fe_settings(u, s)
      get_web_settings(u).deep_merge({
        reduceMotion: s[:reduce_motion],
        autoPlayGif: s[:auto_play_gif],
        displayMedia: s[:display_media],
        expandSpoilers: s[:expand_spoilers],
        unfollowModal: s[:unfollow_modal],
        boostModal: s[:boost_modal],
        deleteModal: s[:delete_modal],
        defaultPrivacy: s[:default_privacy],
        themeMode: map_soapbox_theme_mode(s[:theme]),
        # locale: s[:default_language],
        systemFont: s[:system_font_ui],

        # Mastodon+Soapbox only
        dyslexicFont: s[:dyslexic],
        demetricator: s[:demetricator],
      }).compact
    end

    def get_user_settings(u)
      Setting.find_by_sql(
        "SELECT * FROM settings WHERE thing_type='User' AND thing_id=#{u.id}"
      ).map { |s| [:"#{s.var}", s.value] }.to_h
    end

    def get_web_settings(user)
      begin
        web_settings = ::Web::Setting.where(user: user).first.serializable_hash["data"]
      rescue
        web_settings = {}
      end
      web_settings.compact
    end

    def map_soapbox_theme_mode(theme)
      case theme
      when 'cobalt', 'mastodon-light', 'gabsocial-light', 'neenster', 'glinner'
        'light'
      when 'default', 'contrast', 'halloween'
        'dark'
      else
        nil
      end
    end

    def make_tags(a)
      tags = []
      tags = tags + ["verified"] if a.respond_to?(:is_verified) && a.is_verified
      tags = tags + ["mrf_tag:sandbox"] if a.silenced_at != nil
      tags.length > 0 ? tags : nil
    end
  end
end
