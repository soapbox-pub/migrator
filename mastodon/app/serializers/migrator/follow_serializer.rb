class Migrator::FollowSerializer
  class << self
    include Migrator

    FOLLOW_TYPES = {
      Follow => 'accept',
      FollowRequest => 'pending'
    }

    def to_hash(f)
      a = f.account
      {
        # id: f.id, # Let Pleroma generate it
        data: make_activity(f),
        inserted_at: parse_dt(f.created_at),
        updated_at: parse_dt(f.updated_at),
        local: f.account.local?,
        actor: actor_uri(a),
        recipients: make_recipients(f),
      }
    end

    private

    def make_activity(f)
      data = masto_serialize(ActivityPub::FollowSerializer, f)
      data[:to] = make_to(f)
      data[:cc] = make_cc(f)
      data[:state] = FOLLOW_TYPES[f.class]
      data.freeze
    end

    def make_to(f)
      [actor_uri(f.target_account)]
    end

    def make_cc(f)
      ['https://www.w3.org/ns/activitystreams#Public']
    end

    def make_recipients(f)
      to = make_to(f) || []
      cc = make_cc(f) || []
      (to + cc).uniq
    end
  end
end
