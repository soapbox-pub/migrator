class Migrator::StatusSerializer
  class << self
    include Migrator

    def to_hash(s)
      data = get_activity_hash(s)
      {
        id: s.id,
        data: make_activity(data),
        object_data: make_object_data(data),
        inserted_at: parse_dt(s.created_at),
        updated_at: parse_dt(s.updated_at),
        local: s.local,
        actor: data[:actor],
        recipients: make_recipients(data),
      }
    end

    private

    def get_activity_hash(status)
      c = status.conversation
      context = status_context(status)
      context_id = c.id
      likes = get_likes(status)
      boosts = get_boosts(status)
      data = masto_serialize(ActivityPub::MigratorActivitySerializer, status)
      data[:context_id] = context_id
      data[:context] = context
      data[:directMessage] = status.visibility == "direct"
      if data[:object].is_a? Hash
        data[:object][:context_id] = context_id
        data[:object][:context] = context
        data[:object][:likes] = likes
        data[:object][:like_count] = likes.count
        data[:object][:announcements] = boosts
        data[:object][:announcement_count] = boosts.count
      end
      data.freeze
    end

    def get_likes(s)
      s.favourites.map do |f| actor_uri(f.account) end
    end

    def get_boosts(s)
      s.reblogs.map do |f| actor_uri(f.account) end
    end

    def make_activity(data)
      data = deep_copy(data)
      if data[:object].is_a? Hash
        data[:object] = data[:object][:id]
      end
      data.freeze
    end

    def make_object_data(data)
      data[:object].is_a?(Hash) ? data[:object] : nil
    end

    def make_recipients(data)
      (data[:to] + data[:cc]).uniq
    end
  end
end
