class Migrator::MuteSerializer
  class << self
    include Migrator

    def to_hash(m)
      {
        source_ap_id: actor_uri(m.account),
        target_ap_id: actor_uri(m.target_account),
        inserted_at: parse_dt(m.created_at),
        updated_at: parse_dt(m.updated_at),
      }
    end
  end
end
