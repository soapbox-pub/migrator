class Migrator::AppSerializer
  class << self
    include Migrator

    def to_hash(app)
      {
        client_name: app.name,
        redirect_uris: app.redirect_uri,
        scopes: scopes(app),
        website: app.website,
        client_id: app.uid,
        client_secret: app.secret,
        inserted_at: app.created_at,
        updated_at: app.updated_at,
      }
    end

    private

    def scopes(app)
      return ['read', 'write', 'follow', 'push', 'admin'] if app.id == 1
      return app.scopes.to_a
    end
  end
end
