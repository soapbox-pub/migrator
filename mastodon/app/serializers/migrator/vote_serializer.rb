class Migrator::VoteSerializer
  class << self
    include Migrator

    def to_hash(v)
      data = masto_serialize(ActivityPub::VoteSerializer, v).freeze
      {
        # id: v.id, # Let Pleroma generate it
        data: make_activity(data, v),
        object_data: make_object_data(data),
        inserted_at: parse_dt(v.created_at),
        updated_at: parse_dt(v.updated_at),
        local: v.account.local?,
        actor: actor_uri(v.account),
        recipients: [data[:to]],
      }
    end

    private

    def make_activity(data, v)
      data = deep_copy(data)
      data[:cc] = [data[:to]]
      data[:to] = []
      if data[:object].is_a? Hash
        data[:object] = data[:object][:id]
      end
      data.freeze
    end

    def make_object_data(data)
      object = deep_copy(data)[:object]
      if object.is_a? Hash
        object[:type] = 'Answer'
        object[:to] = []
        object[:cc] = [data[:to]]
        return object.freeze
      end
      nil
    end
  end
end
