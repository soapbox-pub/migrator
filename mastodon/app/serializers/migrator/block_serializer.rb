class Migrator::BlockSerializer
  class << self
    include Migrator

    def to_hash(b)
      {
        # id: f.id, # Let Pleroma generate it
        data: make_activity(b),
        inserted_at: parse_dt(b.created_at),
        updated_at: parse_dt(b.updated_at),
        local: b.account.local?,
        actor: actor_uri(b.account),
        recipients: make_to(b),
      }
    end

    private

    def make_activity(b)
      data = masto_serialize(ActivityPub::BlockSerializer, b)
      data[:to] = make_to(b)
      data.freeze
    end

    def make_to(b)
      [actor_uri(b.target_account)]
    end
  end
end
