class Migrator::LikeSerializer
  class << self
    include Migrator

    def to_hash(f)
      a = f.account
      {
        # id: f.id, # Let Pleroma generate it
        data: make_activity(f),
        inserted_at: parse_dt(f.created_at),
        updated_at: parse_dt(f.updated_at),
        local: f.account.local?,
        actor: actor_uri(a),
        recipients: make_recipients(f),
      }
    end

    private

    def make_activity(f)
      data = masto_serialize(ActivityPub::LikeSerializer, f)
      data[:to] = make_to(f)
      data[:cc] = make_cc(f)
      data[:context] = status_context(f.status)
      data.freeze
    end

    def make_to(f)
      actor_followers_uri = actor_followers_uri(f.account)
      post_author_uri = actor_uri(f.status.account)
      [actor_followers_uri, post_author_uri].compact.uniq
    end

    def make_cc(f)
      mentioned_user_uris = f.status.mentions.map do |m|
        a = m.account
        self_mention = a.id == f.account.id
        self_mention ? nil : actor_uri(a)
      end
      (['https://www.w3.org/ns/activitystreams#Public'] + mentioned_user_uris).compact.uniq
    end

    def make_recipients(f)
      to = make_to(f) || []
      cc = make_cc(f) || []
      (to + cc).uniq
    end
  end
end
