class Migrator::ThreadMuteSerializer
  class << self
    include Migrator

    def to_hash(m)
      {
        ap_id: actor_uri(m.account),
        context: context_uri(m.conversation),
      }
    end
  end
end
