class Migrator::ListSerializer
  class << self
    include Migrator

    def to_hash(l)
      user_ap_id = actor_uri(l.account)
      {
        id: l.id,
        user_ap_id: user_ap_id,
        title: l.title,
        following: l.accounts.map do |a| actor_followers_uri(a) end,
        inserted_at: parse_dt(l.created_at),
        updated_at: parse_dt(l.updated_at),
        ap_id: "#{actor_uri(l.account)}/lists/#{l.id}"
      }
    end
  end
end
