class Migrator::ReportSerializer
  class << self
    include Migrator

    def to_hash(r)
      a = r.account
      {
        # id: r.id, # Let Pleroma generate it
        data: make_activity(r),
        inserted_at: parse_dt(r.created_at),
        updated_at: parse_dt(r.updated_at),
        local: r.account.local?,
        actor: actor_uri(a),
        recipients: make_recipients(r),
      }
    end

    private

    def make_activity(r)
      data = masto_serialize(ActivityPub::FlagSerializer, r)
      data[:to] = make_to(r)
      data[:cc] = make_cc(r)
      data[:state] = r.action_taken ? 'closed' : 'open'
      data.freeze
    end

    def make_to(r)
      []
    end

    def make_cc(r)
      return [] if r.account.id == r.target_account.id
      [actor_uri(r.target_account)]
    end

    def make_recipients(r)
      to = make_to(r) || []
      cc = make_cc(r) || []
      (to + cc).uniq
    end
  end
end
