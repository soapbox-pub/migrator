class Migrator::TokenSerializer
  class << self
    include Migrator

    def to_hash(token)
      {
        app_client_id: token.application.uid,
        user_ap_id: actor_uri(User.find(token.resource_owner_id).account), # No way to speed this up :(
        token: token.token,
        refresh_token: token.refresh_token,
        valid_until: nil,
        inserted_at: token.created_at,
        updated_at: token.created_at,
        scopes: scopes(token),
      }
    end

    private

    def scopes(token)
      return ['read', 'write', 'follow', 'push', 'admin'] if token.application_id == 1
      return token.scopes.to_a
    end
  end
end
