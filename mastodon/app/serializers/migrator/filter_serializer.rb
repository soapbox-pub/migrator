class Migrator::FilterSerializer
  class << self
    include Migrator

    @users = {}

    def to_hash(f)
      ap_id = actor_uri(f.account)
      {
        id: f.id,
        user_ap_id: ap_id,
        filter_id: "SHOULD BE HANDLED BY RAKE TASK",
        hide: f.irreversible,
        phrase: f.phrase,
        context: f.context,
        expires_at: f.expires_at,
        whole_word: f.whole_word,
        inserted_at: parse_dt(f.created_at),
        updated_at: parse_dt(f.updated_at),
      }
    end
  end
end
