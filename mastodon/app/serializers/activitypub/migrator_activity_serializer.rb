# frozen_string_literal: true

# Upgraded ActivitySerializer that handles remote activities
class ActivityPub::MigratorActivitySerializer < ActivityPub::ActivitySerializer
  include Migrator

  def id
    return object.url if object.respond_to?(:local?) && !object.local?
    ActivityPub::TagManager.instance.activity_uri_for(object)
  end

  def to
    case object.visibility
    when 'public'
      [ActivityPub::TagManager::COLLECTIONS[:public]]
    when 'unlisted', 'private'
      [actor_followers_uri(object.account)]
    when 'direct', 'limited'
      if object.account.silenced?
        # Only notify followers if the account is locally silenced
        account_ids = object.active_mentions.pluck(:account_id)
        to = object.account.followers.where(id: account_ids).map { |account| ActivityPub::TagManager.instance.uri_for(account) }
        to.concat(FollowRequest.where(target_account_id: object.account_id, account_id: account_ids).map { |request| ActivityPub::TagManager.instance.uri_for(request.account) })
      else
        object.active_mentions.map { |mention| ActivityPub::TagManager.instance.uri_for(mention.account) }
      end
    end
  end

  def cc
    cc = []

    cc << ActivityPub::TagManager.instance.uri_for(object.reblog.account) if object.reblog?

    case object.visibility
    when 'public'
      cc << actor_followers_uri(object.account)
    when 'unlisted'
      cc << ActivityPub::TagManager::COLLECTIONS[:public]
    end

    unless object.direct_visibility? || object.limited_visibility?
      if object.account.silenced?
        # Only notify followers if the account is locally silenced
        account_ids = object.active_mentions.pluck(:account_id)
        cc.concat(object.account.followers.where(id: account_ids).map { |account| ActivityPub::TagManager.instance.uri_for(account) })
        cc.concat(FollowRequest.where(target_account_id: object.account_id, account_id: account_ids).map { |request| ActivityPub::TagManager.instance.uri_for(request.account) })
      else
        cc.concat(object.active_mentions.map { |mention| ActivityPub::TagManager.instance.uri_for(mention.account) })
      end
    end

    cc
  end
end
