# frozen_string_literal: true

require 'rails_helper'

describe 'Migrator::MuteSerializer' do
  include RoutingHelper

  describe 'to_hash' do
    subject { Fabricate(:mute) }

    it 'can be serialized' do
      Migrator::MuteSerializer.to_hash(subject)
    end
  end
end
