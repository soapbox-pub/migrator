# frozen_string_literal: true

require 'rails_helper'

describe 'Migrator::ListSerializer' do
  include RoutingHelper

  describe 'to_hash' do
    subject { Fabricate(:list) }

    it 'can be serialized' do
      Migrator::ListSerializer.to_hash(subject)
    end
  end
end
