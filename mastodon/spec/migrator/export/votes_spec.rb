# frozen_string_literal: true

require 'rails_helper'

describe 'Migrator::VoteSerializer' do
  include Migrator

  describe 'to_hash' do
    describe 'with a vote from a local user' do
      let(:account) { Fabricate(:account, domain: nil) }
      subject { Fabricate(:poll_vote, account: account) }

      it 'can be serialized' do
        Migrator::VoteSerializer.to_hash(subject)
      end

      it 'has activity type="Create"' do
        activity = Migrator::VoteSerializer.to_hash(subject)
        expect(activity[:data][:type]).to eq 'Create'
      end

      it 'has object type="Answer"' do
        activity = Migrator::VoteSerializer.to_hash(subject)
        expect(activity[:object_data][:type]).to eq 'Answer'
      end

      it 'has an empty "to" field' do
        activity = Migrator::VoteSerializer.to_hash(subject)
        expect(activity[:data][:to]).to eq []
      end
    end

    describe 'with a vote from a remote user' do
      let(:account) { Fabricate(:account, domain: 'foreign.tld') }
      subject { Fabricate(:poll_vote, account: account) }

      it 'can be serialized' do
        Migrator::VoteSerializer.to_hash(subject)
      end
    end
  end
end
