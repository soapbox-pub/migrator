# frozen_string_literal: true

require 'rails_helper'

describe 'Migrator::BlockSerializer' do
  include RoutingHelper

  describe 'to_hash' do
    describe 'with a block from a local user' do
      let(:account) { Fabricate(:account, domain: nil) }
      subject { Fabricate(:block, account: account) }

      it 'can be serialized' do
        Migrator::BlockSerializer.to_hash(subject)
      end

      it 'has "to" field set to the target account' do
        activity = Migrator::BlockSerializer.to_hash(subject)
        expect(activity[:data][:to]).to eq [account_url(subject.target_account)]
      end

      it 'does not have "cc" field' do
        activity = Migrator::BlockSerializer.to_hash(subject)
        expect(activity[:data][:cc].blank?).to be true
      end

      it 'should have type=Block' do
        activity = Migrator::BlockSerializer.to_hash(subject)
        expect(activity[:data][:type]).to eq 'Block'
      end
    end

    describe 'with a block from a remote user' do
      let(:account) { Fabricate(:account, domain: 'foreign.tld') }
      subject { Fabricate(:block, account: account) }

      it 'can be serialized' do
        Migrator::BlockSerializer.to_hash(subject)
      end

      it 'has "to" field set to the target account' do
        activity = Migrator::BlockSerializer.to_hash(subject)
        expect(activity[:data][:to]).to eq [account_url(subject.target_account)]
      end
    end
  end
end
