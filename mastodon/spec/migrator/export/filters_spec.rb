# frozen_string_literal: true

require 'rails_helper'

describe 'Migrator::FilterSerializer' do
  include RoutingHelper

  describe 'to_hash' do
    subject { Fabricate(:custom_filter) }

    it 'can be serialized' do
      Migrator::FilterSerializer.to_hash(subject)
    end
  end
end
