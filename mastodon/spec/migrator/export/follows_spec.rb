# frozen_string_literal: true

require 'rails_helper'

describe 'Migrator::FollowSerializer' do
  include RoutingHelper

  describe 'to_hash' do
    describe 'with a follow from a local user' do
      let(:account) { Fabricate(:account, domain: nil) }
      subject { Fabricate(:follow, account: account) }

      it 'can be serialized' do
        Migrator::FollowSerializer.to_hash(subject)
      end

      it 'has state="accept"' do
        activity = Migrator::FollowSerializer.to_hash(subject)
        expect(activity[:data][:state]).to eq 'accept'
      end
    end

    describe 'with a follow from a remote user' do
      let(:account) { Fabricate(:account, domain: 'foreign.tld') }
      subject { Fabricate(:follow, account: account) }

      it 'can be serialized' do
        Migrator::FollowSerializer.to_hash(subject)
      end
    end

    describe 'with a follow request' do
      subject { Fabricate(:follow_request) }

      it 'has state="pending"' do
        activity = Migrator::FollowSerializer.to_hash(subject)
        expect(activity[:data][:state]).to eq 'pending'
      end
    end
  end
end
