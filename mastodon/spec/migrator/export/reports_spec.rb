# frozen_string_literal: true

require 'rails_helper'

describe 'Migrator::ReportSerializer' do
  include RoutingHelper

  describe 'to_hash' do
    subject { Fabricate(:report) }

    it 'can be serialized' do
      Migrator::ReportSerializer.to_hash(subject)
    end
  end
end
