# frozen_string_literal: true

require 'rails_helper'

describe 'Migrator::ThreadMuteSerializer' do
  include RoutingHelper

  describe 'to_hash' do
    let (:account) { Fabricate(:account) }
    let (:conversation) { Fabricate(:conversation) }
    subject { Fabricate(:conversation_mute, account: account, conversation: conversation) }

    it 'can be serialized' do
      Migrator::ThreadMuteSerializer.to_hash(subject)
    end
  end
end
