# frozen_string_literal: true

require 'rails_helper'

describe 'Migrator::LikeSerializer' do
  include RoutingHelper

  describe 'to_hash' do
    describe 'with a local favourite' do
      let(:account) { Fabricate(:account, domain: nil) }
      subject { Fabricate(:favourite, account: account) }

      it 'can be serialized' do
        Migrator::LikeSerializer.to_hash(subject)
      end
    end

    describe 'with a remote favourite' do
      let(:account) { Fabricate(:account, domain: 'foreign.tld') }
      subject { Fabricate(:favourite, account: account) }

      it 'can be serialized' do
        Migrator::LikeSerializer.to_hash(subject)
      end
    end
  end
end
