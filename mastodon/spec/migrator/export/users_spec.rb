# frozen_string_literal: true

require 'rails_helper'

describe 'Migrator::UserSerializer' do
  include RoutingHelper

  describe 'to_hash' do
    describe 'with a local user' do
      subject { Fabricate(:account, domain: nil) }

      it 'can be serialized' do
        Migrator::UserSerializer.to_hash(subject)
      end
    end

    describe 'with a remote user' do
      subject { Fabricate(:account, domain: 'foreign.tld') }

      it 'can be serialized' do
        Migrator::UserSerializer.to_hash(subject)
      end
    end
  end
end
