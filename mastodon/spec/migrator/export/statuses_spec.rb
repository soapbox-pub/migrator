# frozen_string_literal: true

require 'rails_helper'

describe 'Migrator::StatusSerializer' do
  include RoutingHelper

  describe 'to_hash' do
    describe 'with a local status' do
      let(:account) { Fabricate(:account, domain: nil) }
      subject { Fabricate(:status, account: account, local: true) }

      it 'can be serialized' do
        Migrator::StatusSerializer.to_hash(subject)
      end

      it 'returns the object in linked form' do
        activity = Migrator::StatusSerializer.to_hash(subject)
        expect(activity[:data][:object]).to eq account_status_url(subject.account, subject)
        expect(activity[:object_data].blank?).to be false
      end

      it 'does not have a @context field' do
        activity = Migrator::StatusSerializer.to_hash(subject)
        expect(activity.include? '@context').to be false
      end

      it 'has likes and like_count' do
        Favourite.create!(account: account, status: subject)
        activity = Migrator::StatusSerializer.to_hash(subject.reload)
        expect(activity[:object_data][:like_count]).to eq 1
        expect(activity[:object_data][:likes]).to eq [account_url(account)]
      end

      it 'has announcements and announcement_count' do
        reblog = Fabricate(:status, reblog_of_id: subject.id)
        activity = Migrator::StatusSerializer.to_hash(subject.reload)
        expect(activity[:object_data][:announcement_count]).to eq 1
        expect(activity[:object_data][:announcements]).to eq [account_url(reblog.account)]
      end
    end

    describe 'with a remote status' do
      let(:account) { Fabricate(:account, domain: 'foreign.tld') }
      subject { Fabricate(:status, account: account, local: false) }

      it 'can be serialized' do
        Migrator::StatusSerializer.to_hash(subject)
      end
    end
  end
end
