// Script to download emojis into a format Pleroma can accept.
//
// First visit /api/v1/custom_emojis of your Mastodon server
// and save the output as `emojis.json` in this directory.
//
// Then run `node emoji_dl.js` to download them.
//
// Bringing into Pleroma: https://docs.pleroma.social/backend/configuration/custom_emoji/

const https = require('https');
const fs = require('fs');
const emojis = require('./emojis.json');

const dir = './emojis';
if (!fs.existsSync(dir)) {
  fs.mkdirSync(dir);
}

emojis.forEach(emoji => {
  const ext = emoji.url.split('.').pop();
  const file = fs.createWriteStream(`${dir}/${emoji.shortcode}.${ext}`);
  const request = https.get(emoji.url, function(response) {
    response.pipe(file);
  });
});
