# How it works

- Mastodon is created with [Ruby on Rails](https://rubyonrails.org/) (Ruby), and Pleroma is created with [Phoenix](https://www.phoenixframework.org/) (Elixir).

- This migrator contains Ruby code used by Mastodon to create an export, and Elixir code used by Pleroma to import the data.
