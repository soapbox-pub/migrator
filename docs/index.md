# About Soapbox Migrator

Soapbox Migrator is a tool that can be used to export data from a Mastodon 2.8.4 instance and import it into Pleroma [2.0.5-plerasstodon](https://git.pleroma.social/alexgleason/pleroma/-/tree/plerasstodon).

# Quick Navigation

- [What is migrated?](overview/what-is-migrated)
- [Disclaimer](overview/disclaimer)
- [Philosophy](overview/philosophy)
- [Performing the migration](usage/performing-migration)
