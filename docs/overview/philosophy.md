# Philosophy

The theory for this migrator is that it should be hard to export and easy to import.
It's not meant to be a new standard for interoperable Fediverse databases, it's just meant to solve the use case of Mastodon to Pleroma.
It's a one way function.

We also don't intend for this code to ever be merged upstream.
We're trying to solve the easy problem first instead of the hard one, just to prove it can work, and to keep the finish line closer.

So the goal is to make Mastodon export a format that's as close to what Pleroma needs as possible, then Pleroma will just have to read it without (hopefully) doing anything too crazy.
