# What is migrated?

## What it migrates

- Users
- Statuses
- Follows / Follow Requests
- Likes
- Mutes
- Blocks
- Lists
- Filters
- Thread Mutes
- Polls / Votes
- Frontend settings for [soapbox-fe](https://gitlab.com/soapbox-pub/soapbox-fe)
- OAuth apps/token

## What it doesn't migrate

- Notifications
- Reports
- Moderation log
- Features exclusive to Mastodon or Pleroma
- Frontend settings for any frontend except soapbox-fe
- Remote data that can be redownloaded
- Some remote data that can't be redownloaded
