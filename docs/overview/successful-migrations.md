# Successful Migrations

## gleasonator.com

On May 15, 2020, [gleasonator.com](https://gleasonator.com) was successfully migrated from Mastodon+Soapbox 2.8.4 to Pleroma 2.0.5-plerasstodon.
It resulted in 1.5 hours of downtime with no obvious side-effects.
All following relationships and important local content were retained, and federation continued working as normally.
Read more about it [in my post](https://gleasonator.com/@alex/posts/9v6cIy69oe757CD3iK).

- 41 local users (and 61,173 remote users)
- 21,932 local statuses
- 18,834 local favourites
- 2,726 follows and follow requests
- 45 local blocks
- 209 mutes
- 26 thread mutes
- 367 local poll votes
- 5 lists
- 17 word filters

Challenges:

- I had to perform the migration twice. The first time I accidentally exported `http://` ActivityPub IDs instead of `https://`, a Federation-breaking mistake.

- It took a long time to download the Mastodon database to my laptop. If I did it again, I'd perform the export directly in the production server and download that instead, because the exports don't contain RSA keys for remote users.

- Logged-in users were kicked out of their session after the move, and had to log in again. It might be possible to migrate sessions, but I haven't looked into it yet.

The actual script I used to perform this process can be found in the Soapbox Migrator's Gitlab repo, [`./do_migration.sh`](https://gitlab.com/soapbox-pub/migrator/-/blob/master/do_migration.sh).
I ran it multiple times, destroying the database in between, until I was confident about pointing the DNS at the new server.

## neenster.org

An extremely smooth migration.
It took only 9 minutes of total downtime, and users remained logged in afterwards.

This time, instead of using my laptop as an intermediary between the two servers, I ran the export directly on the Mastodon server, moved the dumps straight to the Pleroma server, then ran the import directly on the Pleroma server.
The old way took 28 minutes, and this way took only 9 minutes.
During the 9 minutes of downtime, I switched the DNS over and got a new SSL cert, so it was completely seamless.

Another improvement is that I got OAuth apps/tokens migrating, so I was able to keep users logged in. This did require [hacking the frontend](https://gitlab.com/soapbox-pub/soapbox-legacy/-/commit/116e886021587fd4c5f01047735da776e64bccf1) of the Mastodon server a week beforehand.

- 56,543 local and remote users
- 3,319 local statuses
- 5,319 local favourites
- Other stuff I didn't count

Challenges:

- After the switch I tested media uploads and they didn't work. I spent at least 2 hours messing with different S3 upload settings before realizing it was actually an incompatibility between an Elixir dependency and the runtime environment... ugh. Patching my Pleroma repo (upgrading the Elixir dependency) fixed it.

## More?

If you've successfully migrated Mastodon to Pleroma with this tool, you're welcome to open a merge request to document your experience here.

Other migrations are planned, including:

- glindr.org
- spinster.xyz
