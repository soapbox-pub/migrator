# Disclaimer

This repo is not for the faint of heart.
If you came here wondering if you can migrate Mastodon to Pleroma, know that it will not be easy.
You should be prepared to learn about the internals of Mastodon/Pleroma, and be willing to edit this repo's code.

Your other option is to just create a new Pleroma server and destroy the old one.
This will be easier in some cases.

That said, this migrator really does work (when operated correctly).
It's designed to be used by large servers and is optimized for speed.
It migrates nearly everything, and can happen very seamlessly.

Long-term, we are aiming to make it as easy as possible, in the hope it can help the Fediverse advance to the next stage.
We believe that Pleroma is the future, and that it will come in all shapes and sizes.
