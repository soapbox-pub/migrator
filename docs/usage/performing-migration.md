# Performing the migration

The gist of it:

1. Set up the new Pleroma server
2. Copy the `pleroma` folder into your Pleroma repo (merge them)
3. Copy the `mastodon` folder into your Mastodon repo (merge them)
4. Shut down Mastodon
5. Perform the export on the Mastodon server (`rake migrator:export`)
6. While this runs, point your domain name DNS to the new server
7. Copy the exports from the Mastodon server to the Pleroma server
8. Perform the import (`mix migrator.import`)
9. ???
10. Profit

You can **skip steps 4 & 6** to test this without downtime.
I highly recommend you do so.
Don't expect this to work the first time.
Verify the exports manually.

See the `do_migration.sh` script for ideas about making it reproducible.
