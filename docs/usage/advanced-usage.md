# Advanced usage and Gulp

You will need local repositories of Mastodon and Pleroma.
You must configure them with the needed environment and make sure they start up properly.
The database you're trying to convert should be loaded into Mastodon.
Note that you MUST include `LOCAL_DOMAIN=yourdomain.tld` and `LOCAL_HTTPS=true` in your Mastodon config, or else it will export localhost URLs.
Pleroma probably needs to be configured similarly.

Create a `.env` file and configure the local paths to Mastodon and Pleroma:

```bash
MASTODON_PATH=/home/alex/Projects/mastodon
PLEROMA_PATH=/home/alex/Projects/pleroma
```

Install dependencies:

```bash
yarn
```

You'll then be able to run these commands to migrate the database:

```bash
yarn masto export
yarn pleroma import
```
