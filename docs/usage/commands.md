# Commands

You can also run more granular commands like this:

```bash
yarn masto hello
yarn pleroma hello

yarn masto export:users
yarn pleroma import.users

yarn masto "export:users[local=true]"
yarn masto "export:statuses[local=true&limit=100]"
```

A query syntax is supported, with these parameters:

- `local`
- `limit`

The commands correspond to [Rake](https://github.com/ruby/rake) tasks and [Mix](https://elixirschool.com/en/lessons/basics/mix/) tasks, for Mastodon and Pleroma respectively.

- **Rake** tasks end in `.rake` and are found in `mastodon/`

- **Mix** tasks end in `.ex` and are found in `pleroma/`

The Yarn commands are shorthand for [gulp](https://gulpjs.com/) commands:

```bash
npx gulp masto --task [rakeTask]
npx gulp pleroma --task [mixTask]
```

Which are in turn shorthand for commands like this:

```bash
cd $MASTODON_PATH && bundle exec rake migrator:[rakeTask]

cd $PLEROMA_PATH && mix migrator.[mixTask]
```

Notice how all tasks are in the `migrator` namespace, and the Yarn command conveniently lets you drop them.
The task files still need that namespace, however.

The benefit of using Yarn is that it will automatically copy the tasks from this repo into the correct location before running them, as well as move the exported data afterwards.
